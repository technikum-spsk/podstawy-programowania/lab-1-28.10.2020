#include <iostream>

using namespace std;

bool isValidTime(int hour, int minute, int second) {
    if (hour > 24 || hour < 0) {
        return false;
    }

    if (minute > 59 || minute < 0) {
        return false;
    }

    if (second > 59 || second < 0) {
        return false;
    }

    return true;
}

void isValidTimeToConsole(bool isValidTime) {
    if (isValidTime) {
        cout << "Czas jest poprawny" << endl;
    } else {
        cout << "Czas jest niepoprawny" << endl;
    }
}

int main() {
    int hour;
    int minute;
    int second;

    cout << "Wpisz godzine: " << endl;
    cin >> hour;
    cout << "Wpisz minute: " << endl;
    cin >> minute;
    cout << "Wpisz sekunde: " << endl;
    cin >> second;

    bool validTime = isValidTime(hour, minute, second);
    cout << validTime << endl;
    isValidTimeToConsole(validTime);

    return 0;
}